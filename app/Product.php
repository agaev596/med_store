<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 
 
class Product extends Model
{
    protected $table = 'products';     
	
    protected $fillable = [
        'id', 'name', 'barcode','enter_price','sell_price','count'
    ];

    
    
}
