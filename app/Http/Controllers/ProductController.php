<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Product as Product;
use App\Http\Services\ProductService as ProductService;

class ProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	private $product;
	private $productSvc;
	
	public function __construct()
	{
		$this->product = new Product();
		$this->productSvc = new ProductService();
	}
	
	public function getProducts()
	{
		$this->product = $this->productSvc->getProducts();
	 	return response()->json(['products'=>$this->product ]);
	}
	   
	public function saveProduct(Request $req)
	{
		$this->productSvc->saveProduct($req);
		return response()->json([
        'name' => $req->all()
     ]);
	}
	
}
