<?php

namespace App\Http\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Product as Product;

class ProductRepository
{
	private $product;
	
	public function __construct()
    {
		$this->product = new Product();
	}
	
	public function saveProduct($req)
	{
		$this->product->fill($req->all());
		$this->product->save();
	}
	
	public function getProducts()
	{
		return Product::all();
	}	
}

?>