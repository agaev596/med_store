<?php

namespace App\Http\Services;
use App\Http\Repositories\ProductRepository as ProductRepo;

class ProductService
{
	private $productRepo;
	
	public function __construct()
	{
		$this->productRepo = new ProductRepo();
	}
	
	public function saveProduct($prod)
	{
		$this->productRepo->saveProduct($prod);
	}
	
	public function getProducts()
	{
		return $this->product = $this->productRepo->getProducts();
	}	
	
}

?>