import Vue from 'vue';
import VueRoutes from 'vue-router'; 
import VueMaterial from 'vue-material'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ExampleComponent from './components/ExampleComponent.vue'
import HomeComponent from './components/HomeComponent.vue'
import AboutComponent from './components/AboutComponent.vue'
import Articles from './components/Articles.vue'
import Products from './components/ProductsComponent.vue'
import AddProduct from './components/AddProductComponent.vue'
import { MdButton, MdContent, MdTabs, MdAppSideDrawer} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'



window.Vue = require('vue');

Vue.use(VueRoutes);
Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)
Vue.use(VueMaterial)
Vue.use(VueAxios, axios)


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
const router = new VueRoutes(
{
	routes : [ 
		{path:'/Home', component : HomeComponent },
		{path:'/Example', component : ExampleComponent },
		{path:'/About', component : AboutComponent},
		{path:'/Products', component : Products},
		{path:'/AddProduct', component : AddProduct}
	]
})
 
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('articles', require('./components/Articles.vue').default);
Vue.component('cards', require('./components/CardComponent.vue').default);
Vue.component('tables', require('./components/TableComponent.vue').default);
Vue.component('add-info', require('./components/AddInfoComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
	router
});
